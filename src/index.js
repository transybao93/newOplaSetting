import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import CORS from 'cors';
import fs from 'fs';
import config from './config.json';
import http from 'http';
import https from 'https';
import settingRouter from './routes/setting';
import bodyParser from 'body-parser';
import logger from './log/logger';
const app = express();
const httpServer = http.createServer(app);
const keysPath = process.env.KEYS_PATH || 'resources';
const useHTTPs = true;

//- using morgan
app.use(morgan(':status [:method] :url - :response-time ms'));

//- using compression
app.use(compression());

//- using CORS for all request
app.use(CORS({
    exposedHeaders: config.corsHeaders
}));

//- body-parser
app.use(bodyParser.json({
	limit : config.bodyLimit
}));
app.use(bodyParser.urlencoded({ 
    extended: true 
}));

//- set all header response type
app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//- setting routers
app.use('/', settingRouter);

//- check if useHTTPs
if(useHTTPs)
{
    //- create https server
    const options = 
    {
        key: fs.readFileSync(`${keysPath}/ssl-keys/privatekey.pem`),
        cert: fs.readFileSync(`${keysPath}/ssl-keys/certificate.pem`)
    };
    app.server = https.createServer(options, app);
}else{
    app.server = http.createServer(app);
}

//- routes
app.get('/:id', (req, res) => {
    let param = req.params['id'];
    res.json({status: 200, message: param})
})

//- open a server
app.server.listen(process.env.PORT || config.port, () => {
    console.log('Server start at port: ' + (process.env.PORT || config.port));
    logger.info('Server start at port: ' + (process.env.PORT || config.port));
})

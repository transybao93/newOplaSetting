import sqlite3 from 'sqlite3';
const sqlite = sqlite3.verbose();
// const sqlite3 = require('sqlite3').verbose();
import fs from 'fs';
import { TABLE_NAME } from '../lib/define';
const RESOURCE_PATH = `${process.cwd()}/resources`
const dbPath        = `${RESOURCE_PATH}/setting.sqlite`;
const db            =  new sqlite.Database(dbPath);
import tokenHelper from '../helper/token';
import emailHelper from '../helper/email';
import passwordHelper from '../helper/password';
import _ from 'lodash';
import logger from '../log/logger';

// Database store in resource/status.sqlite
// export default new sqlite.Database(dbPath);

//- check file
if(!fs.existsSync(dbPath))
{
    logger.info('checking setting.sqlite is not exist');
    new sqlite.Database(dbPath);
    //- create default data
    db.serialize(function () {
        //- opla setting table
        db.each(`select * from ${TABLE_NAME}`, function (err, table) {
            if(typeof table === 'undefined')
            {
                db.serialize(function() {

                    const SERVER_TYPE = {  
                        PROD: 1,  
                        STAG: 2,  
                        DEV: 3
                    }
                    
                    //- create TABLE_NAME table
                    //- expire column with 0 (not expired) and 1 (expired)
                    // db.run(`CREATE TABLE if not exists ${TABLE_NAME} (id INT NOT NULL, version TEXT NOT NULL, status INT NOT NULL, server_gateway TEXT NOT NULL, type TEXT, expire INT DEFAULT 0, maxSupportUser INT DEFAULT 300, currentUserInServer INT DEFAULT 200)`);
                    // const stmt = db.prepare(`INSERT INTO ${TABLE_NAME} VALUES (?,?,?,?,?,?,?,?)`);
                    // stmt.run(1, '1.1', 1, 'https:/proudction.theopla.xyz:3000/1.1/graphql', 'production', 0, 300, 200);
                    // stmt.run(2, '1.1', 1, 'https:/proudction.theopla.xyz:3000/1.1/graphql', 'production', 0, 300, 200);
                    // stmt.run(3, '1.1', 1, 'http://develop.theopla.xyz:3000/1.1/graphql', 'develop', 0, 300, 200);
                    // stmt.run(4, '1.1', 1, 'http://develop.theopla.xyz:3000/1.1/graphql', 'develop', 0, 300, 200);
                    // stmt.run(5, '1.1', 1, 'http://staging.theopla.xyz:3000/1.1/graphql', 'staging', 0, 300, 200);
                    // stmt.run(6, '1.1', 1, 'http://staging.theopla.xyz:3000/1.1/graphql', 'staging', 0, 300, 200);
                    // stmt.finalize();

                    db.run(`CREATE TABLE if not exists ${TABLE_NAME} (version TEXT,type INT,status INT,current_user_support INT,max_user_support INT,server_gateway TEXT)`);
                    const stmt = db.prepare(`INSERT INTO ${TABLE_NAME} VALUES (?,?,?,?,?,?)`);
                    stmt.run('1.1', SERVER_TYPE.PROD, 1,0,10000, 'https:/production.theopla.xyz:3000/1.1/graphql');
                    stmt.run('1.1.1', SERVER_TYPE.PROD, 1,0,10000, 'http://staging.theopla.xyz:3000/1.1.1/graphql');
                    stmt.run('1.1.1', SERVER_TYPE.DEV, 1,0,10000, 'http://develop.theopla.xyz:3000/1.1.1/graphql');
                    stmt.run('1.1.1', SERVER_TYPE.STAG, 1,0,10000, 'http://staging.theopla.xyz:3000/1.1.1/graphql');
                    stmt.finalize();


                    db.each(`SELECT * FROM ${TABLE_NAME}`, function(err, row) {
                        console.log(row);
                        logger.info('new data created: ' + row);
                    });
                    
                });
                // db.close();
            }
        });


        //- token log table
        db.each(`select * from token_log`, function (err, table) {
            if(typeof table === 'undefined')
            {
                db.serialize(function() {

                    //- create token-log table
                    db.run(`CREATE TABLE if not exists token_log (email TEXT, token TEXT, password TEXT)`);
                    const stmt2 = db.prepare(`INSERT INTO token_log VALUES (?,?,?)`);
                    stmt2.run('sample@gmail.com', 'sample', 'sample');
                    stmt2.finalize();


                    db.each(`SELECT * FROM token_log`, function(err, row) {
                        console.log(row);
                    });
                    
                });
                // db.close();
            }
        });


    });
}

//- some other functions
async function updateSetting(maxSupportUser, currentUserInServer, version, status, type, token, res)
{
    try {
        //- verify token
        if(tokenHelper.verifyToken(token))
        {
            logger.info('verify token is OK');
            let emailSent = {};
            emailSent.version = version;
            emailSent.status = status;
            emailSent.type = type;

            let data = [];
            let sql2 = `UPDATE ${TABLE_NAME}
                        SET `
            let field = ``;
            if(maxSupportUser)
            {
                field += `max_user_support=?,`;
                data.push(maxSupportUser);
            }
            if(currentUserInServer)
            {
                field += `current_user_support=?,`;
                data.push(currentUserInServer);
            }
            if(version)
            {
                field += `version=?,`;
                data.push(version);
            }
            if(status)
            {
                field += `status=?,`;
                data.push(status);
            }

            //- remove the last comma
            field = field.slice(0, -1);

            //- push id to the last position in array
            data.push(type);

            let whereQuery = ` WHERE type=?`;
            let finalQuery = sql2 + field + whereQuery;
            console.log('final query: ' + finalQuery)
            
            db.run(finalQuery, data, function(err) {
                if (err) res.status(500).json(err.message);
                
                //- check again
                db.each(`SELECT * FROM ${TABLE_NAME}`, function(err, row) {
                    console.log(row);
                });

                //- decode token
                let tokenDecode = tokenHelper.tokenDecode(token);
                emailSent.email = tokenDecode.email;
                //- send email
                emailHelper.sendEmail('transybao28@gmail.com', emailSent);

                res.status(200).json({
                    status: 200,
                    message: "Update success",
                })
            });
            // db.close();
        }
    } catch (error) {
        return 0
    }
    
}

async function save(email, hashPassword, token)
{
    try {
        await db.serialize(function() {
            // db.run(`CREATE TABLE token-log (email TEXT, token TEXT)`);
            const stmt = db.prepare(`INSERT INTO token_log VALUES (?,?,?)`);
            stmt.run(email, token, hashPassword);
            stmt.finalize();
        
            // db.each(`SELECT * FROM ${TABLE_NAME}`, function(err, row) {
            //     console.log(row);
            // });
            
            // db.close();
        });
        

    } catch (error) {
        return 0;
    }
}

async function viewAllRecord(table_name)
{
    try {
        await db.serialize(function() {
        
            db.each(`SELECT * FROM ${table_name}`, function(err, row) {
                console.log(row);
            });
            
            db.close();
        });

    } catch (error) {
        return false;
    }
}

async function compareEmailToDB(email, password, agent, ip, res)
{
    console.log('here at sqlite.js: ' + email);
    console.log('password: ' + password);
    // email = 'hehe@gmail.com';
    try {

        //- check email then check token
        let sql = `SELECT count(*) count, email, token, password FROM token_log WHERE email = ?`;
        await db.serialize(function() {
            db.each(sql, [email], (err, row) => {
                if(err) return res.status(500).json(err);

                //- if has email then check token expiration
                //- check email with correct password
                if(row.count > 0)
                {
                    //- check password
                    if(passwordHelper.compare(password, row.password))
                    {
                        //- verify token is expired or not
                        if(tokenHelper.verifyToken(row.token))
                        {
                            //- password match
                            return res.status(200).json({
                                message: "email and password match, token is valid and not expired",
                                token: row.token,
                                hashPassword: row.password
                            })
                        }else{
                            return res.status(200).json({
                                message: "token has been expired. Please generate new one"
                            })
                        }
                        
                    }else{
                        //- password is not match
                        return res.status(200).json({
                            message: "password is not match",
                        })
                    }

                }else{
                    let hashedPassword = passwordHelper.generate(password);
                    //- create new token with new data
                    let token = tokenHelper.generate({
                        email: email,
                        agent: agent,
                        ip: ip
                    });

                    //- save to sqlite
                    save(email, hashedPassword, token)

                    return res.status(404).json({
                        message: "Create new account with token",
                        token: token,
                    })
                }
            });
            // db.close();
        });
        
    } catch (error) {
        return res.status(500).json(error)
    }
}

export {
    updateSetting,
    save,
    compareEmailToDB
};

export default new sqlite.Database(dbPath);
import jwt from 'jsonwebtoken';
import tokenHelper from '../helper/token';
import config from '../../src/config.json';
import logger from '../log/logger';

class Auth 
{
    //- verify token from username, password
    async authenticate(req, res, next)
    {
        console.log('email: ' + req.body.email)
        try {
            let email = req.body.email;
            //- generate new token from email
            let token = await tokenHelper.generate({
                email: email
            });
            await logger.info('authentication success and generate token');
            
            next();
        } catch (error) {
            res.status(403).json({
                status: 403,
                message: "Cannot verify your email"
            })
        }
    }
}

export default new Auth();
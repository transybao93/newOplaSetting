import express from 'express';
import checkSetting from '../services/checkSetting';
import { TABLE_NAME } from '../lib/define';
import sqlite, {updateSetting} from '../lib/sqlite';
import logger from '../log/logger';

var router = express.Router();

//- check setting
router.get('/checkSettings', (req, res) => {
    let version = req.query.version;    
    let type = 1;
    logger.info('request to /checkSettings with GET method');
    logger.info('with version: ' + version);
    logger.info('with type: ' + type);
    checkSetting.devSetting(version, sqlite, TABLE_NAME, type, res);
});

router.get('/checkDevSettings', (req, res) => {
    let version = req.query.version;    
    let type = 3;
    logger.info('request to /checkDevSettings with GET method');
    logger.info('with version: ' + version);
    logger.info('with type: ' + type);
    checkSetting.devSetting(version, sqlite, TABLE_NAME, type, res);
});

router.get('/checkStagSettings', (req, res) => {
    let version = req.query.version;
    let type = 2;
    logger.info('request to /checkStagSettings with GET method');
    logger.info('with version: ' + version);
    logger.info('with type: ' + type);
    checkSetting.getSettings(version, sqlite, TABLE_NAME, type, res)
});

//- authentication
router.put('/authentication', (req, res) => {
    let auth = req.body;
    let header = req.headers;
    let userAgent = req.get('User-Agent');
    var ip = req.headers['x-forwarded-for'] || 
     req.connection.remoteAddress || 
     req.socket.remoteAddress ||
     (req.connection.socket ? req.connection.socket.remoteAddress : null);
    
     //- logger
    logger.info('request to /checkDevSettings with PUT method');
    logger.info('with ip: ' + ip);
    logger.info('with userAgent: ' + userAgent);

    checkSetting.authenticate(auth['email'], auth['password'], userAgent, ip, res)

})

//- update version + status
router.put('/updateSetting', (req, res) => {
    let version = req.body['version'];
    let status = req.body['status'];
    let token = req.headers.authorization.split(' ')[1];
    let type = req.body['type'];
    let maxSupportUser = req.body['max_user_support'];
    let currentUserInServer = req.body['current_user_support'];
    
    //- logger
    logger.info('request to /updateSetting with PUT method');
    logger.info('with type: ' + type);
    logger.info('with version: ' + version);

    updateSetting(maxSupportUser, currentUserInServer, version, status, type, token, res)
});
export default router;
import _ from 'lodash';
import fs from 'fs';

class File
{
    checkFileExist(filePath)
    {
        //- check if file is exist
        if (fs.existsSync(filePath)) return 1;
        return 0;
    }
}
export default new File();
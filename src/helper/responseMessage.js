import _ from 'lodash';

class ResponseMessage
{
    //- response
    responseMessage(res, status = 200, message = "Success", data)
    {
        return res.status(status).json({
            status: status,
            message: message,
            data: data
        })
    }
}

export default new ResponseMessage();
import bcrypt from 'bcrypt';

class Password
{
    //- encrypt password
    generate(purePassword){
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(purePassword, salt);
        return hash;
    }

    //- compare two password
    compare(userPass, databasePass){ //- return true/false
        return bcrypt.compareSync(userPass, databasePass); 
    }

}

export default new Password();
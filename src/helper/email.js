import _ from 'lodash';
import nodemailer from 'nodemailer';

class Email
{
    sendEmail(adminEmail, content)
    {
        // let account = {
        //     user: 'opla.testing@gmail.com ',
        //     pass: 'Hr:Gk@9s,@Sy?8/%',
        // };
        let account = {
            user: 'bao988@gmail.com',
            pass: 'transybao',
        };
        let transporter = nodemailer.createTransport(
        {
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            service: 'gmail',
            tls: { rejectUnauthorized: false },
            requireTLS: true,
            auth: {
                user: account.user, // generated ethereal user
                pass: account.pass // generated ethereal password
            }
        }
        // {
        //     service: 'gmail',
        //     tls: { rejectUnauthorized: false },
        //     auth: {
        //       user: 'opla.testing@gmail.com',
        //       pass: 'Hr:Gk@9s,@Sy?8/%',
        //     },
        // }
        
        );
    
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Opla Team" <opla-no-reply@gmail.com>', // sender address
            to: adminEmail, // list of receivers
            subject: 'Tracking update status on server', // Subject line
            text: 'Tracking update status on server', // plain text body
            html: `<p>Update server version tracking</p><br><p>Email: ${content.email}</p><br><p>Version and status updated: ${content.version} - ${content.status}</p><br><p>with type: ${content.type}</p>` // html body
        };
    
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    }
}   

export default new Email();
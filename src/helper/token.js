import jwt from 'jsonwebtoken';
var secret = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPOplaSettingASDFGHJKLZXCVBNM1234567890-=[];',./_+{}:";

class Token{
    /**
     * Generate a token
     */
    generate(userInfo){
        console.log(userInfo);
        let token = jwt.sign(
            userInfo, 
            secret, 
            {expiresIn: "3 days"} //- 3 days
        );
        console.log('token: ' + token);
        return token;
    }

    /**
     * Verify token
     */
    verifyToken(token)
    {
        return jwt.verify(token, secret, function(err, decoded){
            //- if err
            if(err) {
                //- debugging
                if(err.name === 'TokenExpiredError')
                {
                    console.log('Token has been exprired !');
                }
                if(err.name === 'JsonWebTokenError')
                {
                    console.log(err);
                }
                return false;
            }
            //- if ok
            return true;
        });

    }

    /**
     * Decode a token
     */
    tokenDecode(token){
        return jwt.decode(token);
    }

}

export default new Token();

import winston from 'winston';

var options = {
    file: {
        level: 'info',
        name: 'file.info',
        filename: `./app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 2500000, // 2.5MB
        maxFiles: 5,
        colorize: true,
    },
    errorFile: {
        level: 'error',
        name: 'file.error',
        filename: `./error.log`,
        handleExceptions: true,
        json: true,
        maxsize: 2500000, // 2.5MB
        maxFiles: 5,
        colorize: true,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
    };

let logger = winston.createLogger({
    transports: [
        new (winston.transports.Console)(options.console),
        new (winston.transports.File)(options.errorFile),
        new (winston.transports.File)(options.file)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

export default logger;

export const stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
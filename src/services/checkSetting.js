import _ from 'lodash';
import tokenHelper from '../helper/token';
import passwordHelper from '../helper/password';
import { save as sqliteSave, compareEmailToDB } from '../lib/sqlite';
import logger from '../log/logger';


class checkSetting
{
    //- default == 1
    async devSetting(version, sqlite, TABLE_NAME = "", type, res)
    {
        logger.info("..................: " + version);
        const defaultRes = {
            status: 200,
            data: {
                err: 1, 
                message: 'No version for check settings'
            }
        };
        try {
            if (typeof version !== 'string') res.status(defaultRes.status).json(defaultRes.data);
            let query = `SELECT * FROM ${TABLE_NAME} WHERE type = ${type}`;
            return await sqlite.get(query, (err, row) => {
                if (err) res.status(defaultRes.status).json(defaultRes.data);
                const resData = {
                    needUpdate: row.version !== version,
                    status: row.status,
                    serverGateway: row.server_gateway,
                }
                logger.info('response check setting/dev setting')
                logger.info('with data: ' + resData);
                res.status(defaultRes.status).json(resData);
            });
            sqlite.close();
        } catch (error) {
            logger.info('error when response in check setting / check dev setting');
            logger.info('error log: ' + error);
            res.status(500).json(defaultRes.data)
        }
    }

    //- receive user version data
    //- check if user version is outdate or OK
    //- return the information
    //- type will be production, development or staging
    async getSettings(version = "", sqlite, TABLE_NAME = "", type, res)
    {
        const defaultRes = {
            status: 200,
            data: {
                err: 1, 
                message: 'No version to check settings'
            }
        };
        try {
            if (typeof version !== 'string') res.status(defaultRes.status).json(defaultRes.data);
            //- count
            let sql = `SELECT count(version) count, type, version, status, server_gateway, max_user_support, current_user_support FROM ${TABLE_NAME} WHERE type=? AND version=? AND current_user_support < max_user_support`;
            let count = 0;
            return await sqlite.get(sql, [type, version], (error, row) => {
                console.log('count: ' + row.count);
                const resData = {
                    needUpdate: (row.expire === 0) ? row.version !== version : true,
                    status: row.status,
                    serverGateway: row.server_gateway,
                }
                logger.info('response from /getSettings')
                logger.info('with data: ' + resData);
                res.status(defaultRes.status).json(resData);
                // await this.versionCheck(sqlite, defaultRes, TABLE_NAME, type, row.count, res)

            });
            sqlite.close();
        } catch (error) {
            logger.info('error log when call /getSettings: ' + error);
            res.status(500).json(defaultRes.data)
        }
    }

    async versionCheck(sqlite, defaultRes, TABLE_NAME, type, count, res)
    {
        try {
            let sql = "";
            if(count === 0)
            {
                //- if not exist
                //- show back with another type of server if type = production is not found anymore
                //- like staging or development ???
                //- or just staging
                sql = `SELECT * FROM ${TABLE_NAME} WHERE type=? AND current_user_support < max_user_support`;
                type = 'staging';
            }
            else if(count !== 0)
            {
                sql = `SELECT * FROM ${TABLE_NAME} WHERE type=? AND current_user_support < max_user_support`;
            }
            console.log('sql: ' + sql)
            console.log('type: ' + type)
            
            //- give a validation
            await sqlite.serialize(function() {
                sqlite.get(sql, [type], (err, row) => {
                    // if (err) res.status(500).json(defaultRes.data);
                    if(err) console.log('error: ' + err);

                    // res.status(defaultRes.status).json(row);
                    console.log('row: ' + JSON.stringify(row));
                    // res.status(200).json(row);
                    
                    
                });
            });
            // sqlite.close();
        } catch (error) {
            // res.status(500).json(defaultRes.data)
            console.log('error: ' + error);
        }
    }

    //- get token
    async authenticate(email, password, agent, ip, res)
    {

        try {
            let isSimilar = await compareEmailToDB(email, password, agent, ip, res);
            
        } catch (error) {
            res.status(500).json(error)
        }
    }

    //- update setting by generated token
    async updateSetting()
    {
        try {
            
        } catch (error) {
            
        }
    }
}

export default new checkSetting;
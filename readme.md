## Request Optimize/Analyze
1. Compression
2. CORS

## Logger
1. Morgan

## Testing
1. Mocha
2. Chai
3. Chai-http

## Database
1. sqlite3
